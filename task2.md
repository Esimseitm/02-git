# TASK - 2

## STEPS 9-11 in Task-1
1. `git diff` show which new lines added
```
    git diff main
```
2. `git status` show changed files(modified,deleted,added)
```
    git status
```
3.  `git push` pushing local repository to remote repository
```
    git add task1.md
    git commit -m "main - v2"
    git push origin main
```

## STEPS 2-8 in Task-2
1.  Create the `develop` branch and upload the TASK2.MD file into it (Locally and remotetely)
```
    git branch develop
    git checkout develop
    git add .
    git commit -m "develop = v1"
    git push origin develop
```
2. Switch back to the main branch
```
    git checkout main
```
3. Merge from the `develop` branch into the main vera. Resolve conflict in favor of develop branch
```
    git merge develop
```
4. View the log of the changes made
```
    git log
```
**TASK-2 DONE**

## `PS i will do task 3`