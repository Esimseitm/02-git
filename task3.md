# TASK 3

## STEPS 1-15 in Task-3
1. Switch to master branch
```
    git checkout master
```
2. Add changes to the Staging Area
```
    git add .
```
3. Call `git stash` and `git stash list`
```
    git stash
    git stash list
```
4. Switch to develop branch
```
    git checkout develop
```
5. Commit changes and switch back to master
```
    git commit -m "develop -v2"
    git checkout master
```
6. Revert TASK2.MD changes stash back and commit changes
```
    git stash pop
    git add . 
    git commit -m "master -v1"
```
7. Merge develop into master
```
    git merge develop
```
### **TASK-3 DONE**

## *Useful commands*
- `git stash pop` - throws away the(topmost, by default) stash after applying
- `git shash apply` - leaves it in the stash list possible later reuse